package com.kfwebstandard.jspservletmodel2.business;

import java.io.IOException;

import com.kfwebstandard.jspservletmodel2.bean.User;
import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserIO {

    private final static Logger LOG = LoggerFactory.getLogger(UserIO.class);

    public synchronized void addRecord(User user, String filename) throws IOException {
        LOG.debug("Filename = " + filename);
        Path emailList = Paths.get(filename);
        try (BufferedWriter writer = Files.newBufferedWriter(emailList, Charset.forName("UTF-8"), StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(user.getEmail() + "|"
                    + user.getFirstName() + "|"
                    + user.getLastName() + "\n");
        }
    }
}
