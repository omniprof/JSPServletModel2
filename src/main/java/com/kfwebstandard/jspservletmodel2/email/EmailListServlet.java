package com.kfwebstandard.jspservletmodel2.email;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.kfwebstandard.jspservletmodel2.bean.User;
import com.kfwebstandard.jspservletmodel2.business.UserIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailListServlet extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(EmailListServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.jsp";

        // get current action
        String action = request.getParameter("action");
        if (action == null) {
            action = "join";  // default action
        }

        // perform action and set URL to appropriate page
        switch (action) {
            case "join":
                url = "/index.jsp";    // the "join" page
                break;
            case "add":
                // get parameters from the request
                String firstName = request.getParameter("firstName");
                String lastName = request.getParameter("lastName");
                String email = request.getParameter("email");
                // store data in User object
                User user = new User(firstName, lastName, email);
                // validate the parameters
                String message;
                if (firstName == null || lastName == null || email == null
                        || firstName.isEmpty() || lastName.isEmpty() || email.isEmpty()) {
                    message = "Please fill out all three text boxes.";
                    url = "/index.jsp";
                } else {
                    message = null;
                    url = "/thanks.jsp";

                    // Read the context param from web.xml
                    ServletContext context = getServletContext();
                    String fileName = context.getInitParameter("EmailFile");

                    // write the User object to a file
                    UserIO userIO = new UserIO();
                    userIO.addRecord(user, fileName);
                }
                request.setAttribute("user", user);
                request.setAttribute("message", message);
                break;
        }
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
}
